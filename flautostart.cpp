//  See COPYING file for license details.
//  Copyright (C) 2019 Momozor <skelic3@gmail.com>

#include <FL/Fl.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Pack.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Tabs.H>
#include <FL/Fl_Group.H>
#include <FL/Fl_Native_File_Chooser.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Scroll.H>

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <cstdlib>

const std::string AUTOSTART_DESKTOP_PATH = "/etc/xdg/autostart/";
std::map<std::string, int> autostart_list = {
  {"update-menus", 0},
  {"update-notifier", 0},
  {"user-dirs-update-gtk", 0},
  {"pulseaudio", 0},
  {"polkit-gnome-authentication-agent-1", 0},
  {"noscreenblank", 0},
  {"nodpms", 0},
  {"gnome-keyring-ssh", 0},
  {"gnome-keyring-secrets", 0},
  {"gnome-keyring-pkcs11", 0}
};

void make_xdg_list() {
  int i = 0;
  
  for (auto const& [program_name, state]: autostart_list) {
    i = i + 25;
    Fl_Check_Button *b = new Fl_Check_Button(25, i, 150,
                                             25, program_name.c_str());
    b->value(state);
  }
}

Fl_Double_Window* launch_add_window() {
  Fl_Double_Window* top_window =
    new Fl_Double_Window(445, 500, "Add new autostart program");
  Fl_Pack* pack  = new Fl_Pack(0, 25, 445, 500);
  Fl_Button* cancel = new Fl_Button(0, 0, 10, 15, "Cancel");
  Fl_Button* submit = new Fl_Button(0, 25, 10, 15, "Submit");

  pack->end();
  top_window->end();
  top_window->show();

  cancel->callback([](Fl_Widget*, void* tw) {
      Fl_Double_Window* w = static_cast<Fl_Double_Window*>(tw);
      w->hide();
    }, top_window);

  return top_window;
}

void launch_remove_window() {
  Fl_Double_Window* top_window =
    new Fl_Double_Window(250, 250, "Remove autostart program");
  Fl_Pack* pack = new Fl_Pack(0, 25, 350, 445);
  Fl_Button* cancel = new Fl_Button(0, 25, 25, 25, "Cancel");
  Fl_Button* submit = new Fl_Button(0, 25, 25, 25, "Submit");
  
  pack->end();
  top_window->end();
  top_window->show();
}

Fl_Double_Window* make_top_window() {
  Fl_Double_Window* top_window = new Fl_Double_Window(450, 350, "Autostart");

  Fl_Tabs* tabs = new Fl_Tabs(0, 0, 545, top_window->h());
  Fl_Group* jwm = new Fl_Group(0, 20, 540, top_window->h(), "JWM");
  {
    Fl_Button* submit = new Fl_Button(10, 300, 70, 30, "Submit");
    Fl_Button* remove = new Fl_Button(90, 300, 70, 30, "Remove");
  }
  jwm->end();

  Fl_Group* xdg = new Fl_Group(0, 20, 540, top_window->h(), "XDG");
  xdg->hide();
  {
    make_xdg_list();

    Fl_Button* add = new Fl_Button(10, 300, 70, 30, "Add");
    Fl_Button* remove = new Fl_Button(90, 300, 70, 30, "Remove");

    add->callback([](Fl_Widget*, void*) {
        Fl_Native_File_Chooser* fc = new Fl_Native_File_Chooser();
        fc->title("Pick a desktop file");
        fc->type(Fl_Native_File_Chooser::BROWSE_FILE);
        fc->filter("*.desktop");
        fc->directory(AUTOSTART_DESKTOP_PATH.c_str());

        switch (fc->show()) {
        case -1: std::cout << "Error!\n"; break;
        case 1: std::cout << "Cancel!\n"; break;
        default: std::cout << "Picked: " << fc->filename() << std::endl; break;
        }
    });
  }
  xdg->end();
  
  tabs->end();
  top_window->end();
  return top_window;
}

int main(int argc, char *argv[]) {
  make_top_window()->show(argc, argv);
  
  return Fl::run();
}
