CC=gcc
CXX=g++
LDFLAGS=`fltk-config --ldflags`
CXXFLAGS=`fltk-config --cxxflags` -g -std=c++17 -Wall -Wextra 
SOURCE=flautostart.cpp
OUTPUT=flautostart

.PHONY: all clean

all: $(SOURCE)
	$(CXX) -o $(OUTPUT) $(CXXFLAGS) $(SOURCE) $(LDFLAGS)

clean: $(OUTPUT)
	rm -f $(OUTPUT)
